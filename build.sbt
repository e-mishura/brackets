name := "brackets"

version := "0.1"

scalaVersion := "2.12.8"

val catsVersion             = "1.6.0"

scalacOptions ++= Seq(
  "-feature",
  "-deprecation",
  "-unchecked",
  "-language:postfixOps",
  "-language:higherKinds",
  "-Ypartial-unification"
)

libraryDependencies ++= Seq(

  "org.typelevel" %% "cats-core" % catsVersion,
)