import cats.data.State
import cats.implicits._

import scala.annotation.tailrec

trait BracketsSpec {
  def isOpening(b: Char): Boolean

  def isClosing(b: Char): Boolean

  def isMatching(bo: Char, bc: Char): Boolean
}


case class Brackets(brackets: Map[Char, Char]) extends BracketsSpec {

  val cbrackets = brackets.values.toSet

  override def isOpening(b: Char): Boolean = brackets.contains(b)

  override def isClosing(b: Char): Boolean = cbrackets.contains(b)

  override def isMatching(bo: Char, bc: Char): Boolean =
    brackets
      .get(bo)
      .map( _ == bc)
      .getOrElse(false)
}


object Brackets extends App {



  type BStack = List[Char]

  @tailrec
  def validateImpl(bspec: BracketsSpec)(bstack: BStack, input: List[Char]): Boolean = input match {

    case Nil => true
    case c :: tail =>
      if(bspec.isOpening(c))
        validateImpl(bspec)(c :: bstack, tail)
      else if(bspec.isClosing(c)) {
        val isMatch = bstack.headOption
          .map(bspec.isMatching(_, c))
          .getOrElse(false)
        if(isMatch)
          validateImpl(bspec)(bstack.tail, tail)
        else
          false
      }
      else
        validateImpl(bspec)(bstack, tail)

  }


  def validateMImpl(bspec: BracketsSpec)(input: List[Char]): State[BStack, Boolean] =  input match {

    case Nil => State.pure(true)
    case c :: tail =>
      if(bspec.isOpening(c))
        State.modify[BStack](c ::  _) >>  validateMImpl(bspec)(tail)
      else if(bspec.isClosing(c)) for {
        bo <- State.inspect[BStack, Option[Char]](_.headOption)
        isMatch =
        bo.map(bspec.isMatching(_, c))
          .getOrElse(false)

        r <-  if(isMatch)
          State.modify[BStack](_.tail) >> validateMImpl(bspec)(tail)
        else
          State.pure[BStack, Boolean](false)
      } yield r
      else
        validateMImpl(bspec)(tail)

  }

  def validate(bspec: BracketsSpec)(input: String): Boolean =
    validateImpl(bspec)(Nil, input.toList)
  def validateM(bspec: BracketsSpec)(input: String): Boolean =
    validateMImpl(bspec)(input.toList).runA(Nil).value


  val bspec =Brackets(Map(
    '{' -> '}',
    '[' -> ']'
  ))
  val myValidate = validate(bspec) _

  println(myValidate("{}[]"))
  println(myValidate("{xx{[f]xx}cccxc}[]"))
  println(myValidate("{[}]"))

  val myValidateM = validateM(bspec) _

  println(myValidateM("{}[]"))
  println(myValidateM("{xx{[f]xx}cccxc}[]"))
  println(myValidateM("{[}]"))

}
